package com.pengu.betterchat;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.client.resources.DefaultPlayerSkin;
import net.minecraft.util.ResourceLocation;

import com.pengu.hammercore.client.utils.SkinUtils;

public class SkinGetter
{
	private static final Map<String, ResourceLocation> SKIN_MAP = new HashMap<>();
	
	public static ResourceLocation cacheSkin(String user)
	{
		if(SKIN_MAP.containsKey(user))
			return SKIN_MAP.get(user);
		NetworkPlayerInfo n = Minecraft.getMinecraft().getConnection().getPlayerInfo(user);
		if(n != null)
		{
			ResourceLocation r;
			SKIN_MAP.put(user, r = SkinUtils.getSkinTexture(n.getGameProfile()));
			return r;
		}
		return DefaultPlayerSkin.getDefaultSkinLegacy();
	}
}