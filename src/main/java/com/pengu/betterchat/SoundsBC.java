package com.pengu.betterchat;

import com.pengu.hammercore.utils.SoundObject;

public class SoundsBC
{
	public static final SoundObject //
	        INCOMING_CALL = new SoundObject(Info.MOD_ID, "incoming_call"), //
	        PRIVATE_MESSAGE = new SoundObject(Info.MOD_ID, "private_message");
}