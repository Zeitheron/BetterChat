package com.pengu.betterchat.tasks;

import com.pengu.betterchat.NotificationManager;
import com.pengu.betterchat.messages.vcm.VoiceCall;
import com.pengu.hammercore.net.packetAPI.p2p.iTask;

import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class TaskStartVoiceCall implements iTask
{
	public String remote;
	
	public TaskStartVoiceCall(String to)
	{
		remote = Minecraft.getMinecraft().getSession().getUsername();
		if(NotificationManager.currentCall != null)
			throw new RuntimeException("Can't start call: you are already talking!");
		NotificationManager.currentCall = new VoiceCall(to);
	}
	
	public TaskStartVoiceCall()
	{
	}
	
	@Override
	public void execute(MessageContext arg0)
	{
		NotificationManager.pending_calls.put(remote, new VoiceCall(remote));
		
		boolean b = NotificationManager.makeSound;
		NotificationManager.makeSound = false;
		NotificationManager.makeNotification(remote + " is calling you...");
		NotificationManager.makeSound = b;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound arg0)
	{
		remote = arg0.getString("r");
	}
	
	@Override
	public void writeToNBT(NBTTagCompound arg0)
	{
		arg0.setString("r", remote);
	}
}