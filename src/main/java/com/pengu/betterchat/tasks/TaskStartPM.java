package com.pengu.betterchat.tasks;

import com.pengu.betterchat.messages.Message;
import com.pengu.betterchat.messages.MessageList;
import com.pengu.betterchat.messages.MessageLoader;
import com.pengu.hammercore.net.packetAPI.p2p.iTask;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class TaskStartPM implements iTask
{
	String sender;
	String message;
	
	public TaskStartPM(String sender, String message)
	{
		this.sender = sender;
		this.message = message;
	}
	
	public TaskStartPM()
	{
	}
	
	@Override
	public void execute(MessageContext arg0)
	{
		MessageList list = MessageLoader.list;
		if(list == null)
			list = new MessageList();
		list.getOrStartHistory(sender).addMessage(new Message(null, message));
	}
	
	@Override
	public void readFromNBT(NBTTagCompound arg0)
	{
		sender = arg0.getString("s");
		message = arg0.getString("m");
	}
	
	@Override
	public void writeToNBT(NBTTagCompound arg0)
	{
		arg0.setString("s", sender);
		arg0.setString("m", message);
	}
}