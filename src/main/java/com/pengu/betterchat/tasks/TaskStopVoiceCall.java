package com.pengu.betterchat.tasks;

import com.pengu.betterchat.NotificationManager;
import com.pengu.betterchat.messages.vcm.VoiceCall;
import com.pengu.hammercore.net.packetAPI.p2p.iTask;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class TaskStopVoiceCall implements iTask
{
	public TaskStopVoiceCall()
    {
    }
	
	public TaskStopVoiceCall(String unused)
	{
		VoiceCall call = NotificationManager.currentCall;
		if(call != null)
		{
			call.disconnect();
			NotificationManager.currentCall = null;
		}
		else
			throw new RuntimeException("You are not talking!");
		NotificationManager.currentCall = null;
	}
	
	@Override
	public void execute(MessageContext arg0)
	{
		VoiceCall call = NotificationManager.currentCall;
		if(call != null)
			call.disconnect();
		NotificationManager.currentCall = null;
	}
	
	@Override
	public void readFromNBT(NBTTagCompound arg0)
	{
	}
	
	@Override
	public void writeToNBT(NBTTagCompound arg0)
	{
	}
}