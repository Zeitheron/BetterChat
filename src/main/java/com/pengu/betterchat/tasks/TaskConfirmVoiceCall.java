package com.pengu.betterchat.tasks;

import com.pengu.betterchat.NotificationManager;
import com.pengu.betterchat.messages.Message;
import com.pengu.betterchat.messages.MessageList;
import com.pengu.betterchat.messages.MessageLoader;
import com.pengu.betterchat.messages.vcm.VoiceCall;
import com.pengu.hammercore.net.HCNetwork;
import com.pengu.hammercore.net.packetAPI.p2p.iTask;

import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class TaskConfirmVoiceCall implements iTask
{
	public String error;
	
	public TaskConfirmVoiceCall(String err)
	{
		if(err != null)
		{
			String un = Minecraft.getMinecraft().getSession().getUsername();
			String len = un.length() + "";
			while(len.length() < 3)
				len = "0" + len;
			un = len + un;
			err = un + err;
		}
		error = err;
		if(err == null && NotificationManager.currentCall != null)
			NotificationManager.currentCall.init();
	}
	
	public TaskConfirmVoiceCall()
	{
	}
	
	@Override
	public void execute(MessageContext arg0)
	{
		String user = null;
		if(error != null)
		{
			int len = Integer.parseInt(error.substring(0, 3));
			user = error.substring(3, 3 + len);
			error = error.substring(len + 3);
		}
		
		if(error != null)
		{
			MessageList list = MessageLoader.list;
			if(list != null)
				list.getOrStartHistory(user).addMessage(new Message(null, error));
			NotificationManager.makeNotification(error);
		}
		
		VoiceCall vc = NotificationManager.currentCall;
		if(vc != null)
		{
			vc.error$ = error;
			if(error != null)
			{
				vc.disconnect();
				try
				{
					HCNetwork.p2p.sendTo(new TaskStopVoiceCall(error), vc.remoteUser);
				} catch(RuntimeException err)
				{
				}
			} else
				try
				{
					vc.init();
				} catch(Throwable err)
				{
				}
		}
	}
	
	@Override
	public void readFromNBT(NBTTagCompound arg0)
	{
		if(arg0.hasKey("er"))
			error = arg0.getString("er");
		else
			error = null;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound arg0)
	{
		if(error != null)
			arg0.setString("er", error);
	}
}