package com.pengu.betterchat.tasks;

import com.pengu.betterchat.NotificationManager;
import com.pengu.betterchat.messages.vcm.VoiceCall;
import com.pengu.hammercore.net.packetAPI.p2p.iTask;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class TaskTransferVoiceData implements iTask
{
	public byte[] data;
	
	public TaskTransferVoiceData(byte[] data)
	{
		this.data = data;
	}
	
	public TaskTransferVoiceData()
	{
	}
	
	@Override
	public void execute(MessageContext arg0)
	{
		VoiceCall c = NotificationManager.currentCall;
		if(c != null)
			c.feedInAudio(data);
	}
	
	@Override
	public void writeToNBT(NBTTagCompound arg0)
	{
		arg0.setByteArray("d", data);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound arg0)
	{
		data = arg0.getByteArray("d");
	}
}