package com.pengu.betterchat.messages;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageList implements Serializable
{
	private Map<String, MessageHistory> histories = new HashMap<>();
	private List<String> usersUNM = null;
	
	public MessageHistory getOrStartHistory(String username)
	{
		if(histories.containsKey(username))
			return histories.get(username);
		MessageHistory history = new MessageHistory();
		history.user = username;
		histories.put(username, history);
		return history;
	}
	
	public List<String> getAllHistories()
	{
		/** Make an unmodifiable list of variables */
		if(usersUNM == null || usersUNM.size() != histories.keySet().size())
			usersUNM = Arrays.asList(histories.keySet().toArray(new String[histories.keySet().size()]));
		return usersUNM;
	}
}