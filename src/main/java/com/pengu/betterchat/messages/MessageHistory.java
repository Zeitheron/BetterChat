package com.pengu.betterchat.messages;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;

import com.pengu.betterchat.NotificationManager;
import com.pengu.betterchat.gui.GuiPMChat;

public class MessageHistory implements Serializable
{
	public List<Message> messages = new ArrayList<>();
	public String user;
	
	public void addMessage(Message e)
	{
		List<Message> cop = new ArrayList<>(messages.size() + 1);
		cop.add(e);
		cop.addAll(messages);
		messages = cop;
		
		String cut = e.message.substring(0, Math.min(100, e.message.length()));
		
		if(e.sender != null)
		{
			if(Minecraft.getMinecraft().currentScreen instanceof GuiPMChat)
			{
				GuiPMChat chat = (GuiPMChat) Minecraft.getMinecraft().currentScreen;
				/* Prevent receiving excessive notifications */
				if(chat.history.user.equals(user))
					return;
			}
			NotificationManager.makeNotification(e.sender + ":\n" + cut + (cut.equals(e.message) ? "" : "..."));
		}
	}
}