package com.pengu.betterchat.messages.vcm;

public enum EnumVoicePhase
{
	NOT_INITIALIZED,
	INITIALIZED,
	ERRORED,
	DISCONNECTED;
}