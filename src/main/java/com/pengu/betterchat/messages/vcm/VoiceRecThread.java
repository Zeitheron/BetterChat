package com.pengu.betterchat.messages.vcm;

import java.io.ByteArrayOutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Arrays;

import com.pengu.betterchat.NotificationManager;
import com.pengu.betterchat.tasks.TaskTransferVoiceData;
import com.pengu.hammercore.net.HCNetwork;

import net.minecraft.util.math.MathHelper;

public class VoiceRecThread extends Thread
{
	private boolean allowedToRun = true;
	public final VoiceCall call;
	
	public VoiceRecThread(VoiceCall call)
	{
		this.call = call;
	}
	
	public boolean $Join()
	{
		try
		{
			allowedToRun = false;
			join(500);
			if(isAlive())
				interrupt();
			return true;
		} catch(Throwable err)
		{
		}
		return false;
	}
	
	@Override
	public void run()
	{
		int numBytesRead;
		int CHUNK_SIZE = 1024;
		byte[] data = new byte[call.microphone.getBufferSize() / 5];
		call.microphone.start();
		
		long lastMeasure = System.currentTimeMillis();
		int bps = 0;
		
		try
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			while(allowedToRun)
			{
				if(call != NotificationManager.currentCall)
					break;
				numBytesRead = call.microphone.read(data, 0, CHUNK_SIZE);
				
//				for(int i = numBytesRead % 2; i < numBytesRead;)
//				{
//					int sample = 0;
//					sample |= data[i++] & 0xFF;
//					sample |= data[i++] << 8;
//					float s = MathHelper.abs(sample / 32768F);
//					
//					if(s < .05F)
//						data[i - 1] = data[i] = 0;
//				}
				
				baos.write(data, 0, numBytesRead);
				
				if(System.currentTimeMillis() - lastMeasure >= 75L)
				{
					lastMeasure = System.currentTimeMillis();
					byte[] tickData = baos.toByteArray();
					baos.reset();
					
					if(!call.canSpeak)
						Arrays.fill(tickData, (byte) 0);
					
					HCNetwork.p2p.sendTo(new TaskTransferVoiceData(tickData), call.remoteUser);
				}
			}
		} catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}