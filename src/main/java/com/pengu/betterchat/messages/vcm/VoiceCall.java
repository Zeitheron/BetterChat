package com.pengu.betterchat.messages.vcm;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

import net.minecraft.client.Minecraft;

import com.pengu.betterchat.BetterChatMod;

public class VoiceCall
{
	private List<byte[]> dataBuffer = new ArrayList<>();
	
	private EnumVoicePhase phase = EnumVoicePhase.NOT_INITIALIZED;
	public String error$;
	public final PipedInputStream voiceInput;
	public final PipedOutputStream voiceOutput;
	public AudioFormat format = new AudioFormat(16000F, 16, 1, true, false);
	public TargetDataLine microphone;
	private VoicePlayThread volThread;
	private VoiceRecThread recThread;
	public boolean canHear = true, canSpeak = true;
	public int timeout = 20 * 15 /* 15 seconds */;
	
	public final String clientUser, remoteUser;
	
	public VoiceCall(String remoteUser)
	{
		this.remoteUser = remoteUser;
		clientUser = Minecraft.getMinecraft().getSession().getUsername();
	}
	
	{
		voiceInput = new PipedInputStream(1024 * 16);
		PipedOutputStream pos = null;
		try
		{
			pos = new PipedOutputStream(voiceInput);
		} catch(IOException e)
		{
			e.printStackTrace();
		}
		voiceOutput = pos;
	}
	
	public EnumVoicePhase getPhase()
	{
		return phase;
	}
	
	public void feedInAudio(byte[] data)
	{
		/** Make audio not lose connection between server <--> client */
		dataBuffer.add(data);
		synchronized(dataBuffer)
		{
			dataBuffer.notifyAll();
		}
		while(dataBuffer.size() > 1)
			dataBuffer.remove(0);
	}
	
	public void init()
	{
		if(phase == EnumVoicePhase.NOT_INITIALIZED)
		{
			DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
			
			try
			{
				microphone = AudioSystem.getTargetDataLine(format);
				microphone = (TargetDataLine) AudioSystem.getLine(info);
				microphone.open(format);
				
				volThread = new VoicePlayThread(this);
				recThread = new VoiceRecThread(this);
				
				volThread.setName("Voice Call Player");
				volThread.start();
				
				recThread.setName("Voice Rec Thread");
				recThread.start();
				
				new Thread(() ->
				{
					Thread.currentThread().setName("Buffer Cache Thread for Voice Call");
					
					while(true)
					{
						synchronized(dataBuffer)
						{
							try
							{
								dataBuffer.wait();
							} catch(Throwable err)
							{
								err.printStackTrace();
							}
						}
						
						while(!dataBuffer.isEmpty())
							try
							{
								byte[] data = dataBuffer.remove(0);
								voiceOutput.write(data);
							} catch(Throwable err)
							{
							}
					}
				}).start();
			} catch(LineUnavailableException err)
			{
				BetterChatMod.LOG.error("ERROR: Unable to find microphone audio line!");
				err.printStackTrace();
				error$ = "Unable to find microphone audio line!";
				phase = EnumVoicePhase.ERRORED;
			} catch(Throwable err)
			{
				BetterChatMod.LOG.error("ERROR: Failed to initialize microphone audio line!");
				err.printStackTrace();
				error$ = err.getMessage();
				phase = EnumVoicePhase.ERRORED;
			}
			
			phase = EnumVoicePhase.INITIALIZED;
		} else
			throw new RuntimeException("Unable to initialize in phase " + phase);
	}
	
	public void disconnect()
	{
		if(phase != EnumVoicePhase.DISCONNECTED)
		{
			if(volThread != null)
				volThread.$Join();
			if(recThread != null)
				recThread.$Join();
			volThread = null;
			recThread = null;
			if(microphone != null)
				microphone.close();
			phase = EnumVoicePhase.DISCONNECTED;
		}
	}
}