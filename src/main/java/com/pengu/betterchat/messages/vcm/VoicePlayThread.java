package com.pengu.betterchat.messages.vcm;

import java.util.Arrays;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import com.pengu.betterchat.NotificationManager;

import net.minecraft.util.math.MathHelper;

public class VoicePlayThread extends Thread
{
	private boolean allowedToRun = true;
	public final VoiceCall call;
	
//	public float sample;
	
	public VoicePlayThread(VoiceCall call)
	{
		this.call = call;
	}
	
	public boolean $Join()
	{
		try
		{
			allowedToRun = false;
			join(500);
			if(isAlive())
				interrupt();
			return true;
		} catch(Throwable err)
		{
		}
		return false;
	}
	
	@Override
	public void run()
	{
		try
		{
			SourceDataLine sourceDataLine;
			
			AudioInputStream audioInputStream = new AudioInputStream(call.voiceInput, call.format, AudioSystem.NOT_SPECIFIED);
			DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, call.format);
			sourceDataLine = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
			sourceDataLine.open(call.format);
			sourceDataLine.start();
			int cnt = 0;
			byte tempBuffer[] = new byte[32];
			try
			{
				while(allowedToRun)
				{
					if(call != NotificationManager.currentCall)
						break;
					cnt = audioInputStream.read(tempBuffer, 0, tempBuffer.length);
					if(cnt > 0)
					{
						if(!call.canHear)
							Arrays.fill(tempBuffer, (byte) 0);
						sourceDataLine.write(tempBuffer, 0, cnt);
						
						int sample = 0;
						sample |= tempBuffer[0] & 0xFF;
						sample |= tempBuffer[1] << 8;
						float s = MathHelper.abs(sample / 32768F);
					} else
						Thread.sleep(5L);
				}
			} catch(Exception e)
			{
				e.printStackTrace();
			}
			sourceDataLine.drain();
			sourceDataLine.close();
		} catch(LineUnavailableException err)
		{
		}
	}
}