package com.pengu.betterchat.messages;

import java.io.Serializable;

public class Message implements Serializable
{
	/** If {@link #sender} == null, the message will be centered */
	public String sender, message;
	
	public Message(String sender, String message)
    {
		this.sender = sender;
		this.message = message;
    }
	
	public Message()
    {
    }
}