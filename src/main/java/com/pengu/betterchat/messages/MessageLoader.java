package com.pengu.betterchat.messages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import com.pengu.betterchat.BetterChatMod;

import net.minecraft.client.Minecraft;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class MessageLoader
{
	public static MessageList list = new MessageList();
	
	public static void saveMessages()
	{
		if(list == null)
			list = new MessageList();
		File bc = new File("pengu-betterchat", "histories");
		if(!bc.isDirectory())
			bc.mkdirs();
		File ml = new File(bc, BetterChatMod.lastWorldName + ".chl");
		try(ObjectOutputStream o = new ObjectOutputStream(new GZIPOutputStream(new FileOutputStream(ml)));)
		{
			o.writeObject(list);
		} catch(Throwable err)
		{
			err.printStackTrace();
		}
	}
	
	public static void loadMessages()
	{
		File bc = new File("pengu-betterchat", "histories");
		if(!bc.isDirectory())
			bc.mkdirs();
		File ml = new File(bc, BetterChatMod.lastWorldName + ".chl");
		try(ObjectInputStream i = new ObjectInputStream(new GZIPInputStream(new FileInputStream(ml)));)
		{
			list = (MessageList) i.readObject();
		} catch(FileNotFoundException err)
		{
			BetterChatMod.LOG.warn("Could not locate chat history for world \"" + BetterChatMod.lastWorldName + "\", creating empty history.");
			list = new MessageList();
			saveMessages();
		} catch(Throwable err)
		{
			err.printStackTrace();
			list = new MessageList();
		}
	}
}