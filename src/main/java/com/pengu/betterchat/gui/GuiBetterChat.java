package com.pengu.betterchat.gui;

import java.io.IOException;
import java.util.List;

import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import com.pengu.betterchat.Info;
import com.pengu.betterchat.NotificationManager;
import com.pengu.betterchat.SkinGetter;
import com.pengu.betterchat.messages.MessageHistory;
import com.pengu.betterchat.messages.MessageList;
import com.pengu.betterchat.messages.MessageLoader;
import com.pengu.betterchat.messages.vcm.VoiceCall;
import com.pengu.betterchat.tasks.TaskStopVoiceCall;
import com.pengu.hammercore.math.MathHelper;
import com.pengu.hammercore.net.HCNetwork;

public class GuiBetterChat extends GuiScreen
{
	public static final ResourceLocation widgets = new ResourceLocation(Info.MOD_ID, "textures/widgets.png");
	public static final ResourceLocation disableMic = new ResourceLocation(Info.MOD_ID, "textures/disable_microphone.png");
	public static final ResourceLocation disableSpk = new ResourceLocation(Info.MOD_ID, "textures/disable_speakers.png");
	
	public float cancelDepth = 0;
	int offset = 0;
	protected int mouseX, mouseY;
	
	@Override
	public void initGui()
	{
		super.initGui();
	}
	
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		this.mouseX = mouseX;
		this.mouseY = mouseY;
		
		drawGradientRect(0, 0, width, height, 0xCCFFFFFF, 0xCCFFFFFF);
		
		GlStateManager.enableBlend();
		
		MessageList list = MessageLoader.list;
		List<String> hst = list.getAllHistories();
		
		int mw = 0;
		for(int j = offset; j < Math.min(16 + offset, hst.size()); ++j)
		{
			int i = j - offset;
			String u = hst.get(j);
			MessageHistory history = list.getOrStartHistory(u);
			mc.getTextureManager().bindTexture(SkinGetter.cacheSkin(u));
			
			if(mouseX < 36 + fontRenderer.getStringWidth(u) + 2 && mouseY >= i * 34 && mouseY < (i + 1) * 34)
				drawGradientRect(0, i * 34, 36 + fontRenderer.getStringWidth(u) + 2, (i + 1) * 34, 0x7755FF55, 0x7755FF55);
			
			GL11.glPushMatrix();
			GL11.glTranslated(0, i * 34 + 1, 0);
			drawTexturedModalRect(0, 0, 32, 32, 32, 32);
			GL11.glPopMatrix();
			GL11.glColor4d(0, 0, 0, 1);
			fontRenderer.drawString(u, 36, i * 34 + 1, 0, false);
			mw = Math.max(mw, 38 + fontRenderer.getStringWidth(u));
			GL11.glColor4d(1, 1, 1, 1);
		}
		
		drawGradientRect(mw, 0, mw + 2, height, 0xCCCCCCCC, 0xCCCCCCCC);
		
		if(NotificationManager.currentCall != null)
		{
			drawGradientRect(mw, 0, width, height, 0xCCCCCCCC, 0xCCCCCCCC);
			
			VoiceCall call = NotificationManager.currentCall;
			
			mc.getTextureManager().bindTexture(disableMic);
			
			GL11.glPushMatrix();
			GL11.glColor4d(NotificationManager.currentCall.canSpeak ? 0 : 1, 0, 0, 1);
			GL11.glTranslated((width + mw + 2) / 2 - 33, height * .8, 0);
			GL11.glScaled(2 / 16D, 2 / 16D, 2 / 16D);
			GlStateManager.enableBlend();
			drawTexturedModalRect(0, 0, 0, 0, 256, 256);
			GL11.glPopMatrix();
			
			mc.getTextureManager().bindTexture(disableSpk);
			
			GL11.glPushMatrix();
			GL11.glColor4d(NotificationManager.currentCall.canHear ? 0 : 1, 0, 0, 1);
			GL11.glTranslated((width + mw + 2) / 2 + 1, height * .8, 0);
			GL11.glScaled(2 / 16D, 2 / 16D, 2 / 16D);
			GlStateManager.enableBlend();
			drawTexturedModalRect(0, 0, 0, 0, 256, 256);
			GL11.glPopMatrix();
			
			GL11.glColor4d(1, 1, 1, 1);
			
			mc.getTextureManager().bindTexture(SkinGetter.cacheSkin(NotificationManager.currentCall.remoteUser));
			GL11.glPushMatrix();
			GL11.glTranslated((width + mw) / 2 - 33, height / 2 - 16, 0);
			drawTexturedModalRect(0, 0, 32, 32, 32, 32);
			GL11.glPopMatrix();
			
			mc.getTextureManager().bindTexture(SkinGetter.cacheSkin(NotificationManager.currentCall.clientUser));
			GL11.glPushMatrix();
			GL11.glTranslated((width + mw) / 2 + 1, height / 2 - 16, 0);
			drawTexturedModalRect(0, 0, 32, 32, 32, 32);
			GL11.glPopMatrix();
			
			double cm = mouseX >= (width + mw) / 2 - 16 && mouseX < (width + mw) / 2 + 16 && mouseY >= height * .8 - 32 && mouseY < height * .8 ? 1 : .7;
			mc.getTextureManager().bindTexture(GuiPMChat.phoneCall);
			GL11.glPushMatrix();
			GL11.glTranslated((width + mw) / 2 - 16, height * .8 - 32, 0);
			GL11.glScaled(2 / 16D, 2 / 16D, 2 / 16D);
			GL11.glColor3d(1 * cm, .25 * cm, .25 * cm);
			drawTexturedModalRect(0, 0, 0, 0, 256, 256);
			GL11.glPopMatrix();
			
			if(mouseX >= (width + mw) / 2 - 33 && mouseX < (width + mw) / 2 && mouseY >= height / 2 - 16 && mouseY < height / 2 + 16)
				drawHoveringText(NotificationManager.currentCall.remoteUser + " (Partner)", mouseX, mouseY);
			
			if(mouseX >= (width + mw) / 2 + 1 && mouseX < (width + mw) / 2 + 33 && mouseY >= height / 2 - 16 && mouseY < height / 2 + 16)
				drawHoveringText(NotificationManager.currentCall.clientUser + " (You)", mouseX, mouseY);
		}
		
		GlStateManager.enableBlend();
		
		mc.getTextureManager().bindTexture(widgets);
		GL11.glColor3f(cancelDepth, 0, 0);
		drawTexturedModalRect(width - 10, 0, 0, 0, 10, 10);
		GL11.glColor3f(0, 0, 0);
		drawTexturedModalRect(width - 25, 0, 10, 0, 10, 10);
		drawTexturedModalRect(width - 40, 0, 0, 10, 10, 10);
		GL11.glColor3f(1, 1, 1);
		
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	@Override
	public void updateScreen()
	{
		super.updateScreen();
		if(mouseX >= width - 10 && mouseX < width && mouseY >= 0 && mouseY < 10)
			cancelDepth += .1F;
		else
			cancelDepth -= .25F;
		cancelDepth = (float) MathHelper.clip(cancelDepth, 0, 1);
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		
		MessageList list = MessageLoader.list;
		List<String> hst = list.getAllHistories();
		
		int mw = 0;
		for(int j = offset; j < Math.min(16 + offset, hst.size()); ++j)
		{
			int i = j - offset;
			String u = hst.get(j);
			if(mouseX < 36 + fontRenderer.getStringWidth(u) + 2 && mouseY >= i * 34 && mouseY < (i + 1) * 34)
			{
				mc.displayGuiScreen(new GuiPMChat(this, list.getOrStartHistory(u)));
				mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_TOAST_IN, 1));
				return;
			}
			mw = Math.max(mw, 38 + fontRenderer.getStringWidth(u));
		}
		
		if(NotificationManager.currentCall != null)
		{
			if(mouseX >= (width + mw) / 2 - 16 && mouseX < (width + mw) / 2 + 16 && mouseY >= height * .8 - 32 && mouseY < height * .8)
			{
				String rem = NotificationManager.currentCall.remoteUser;
				HCNetwork.p2p.sendTo(new TaskStopVoiceCall(null), rem);
			}
			
			if(mouseX >= (width + mw + 2) / 2 - 33 && mouseX < (width + mw + 2) / 2 - 1 && mouseY >= height * .8 && mouseY < height * .8 + 32)
				NotificationManager.currentCall.canSpeak = !NotificationManager.currentCall.canSpeak;
			
			if(mouseX >= (width + mw + 2) / 2 + 1 && mouseX < (width + mw + 2) / 2 + 33 && mouseY >= height * .8 && mouseY < height * .8 + 32)
				NotificationManager.currentCall.canHear = !NotificationManager.currentCall.canHear;
		}
		
		if(mouseX >= width - 25 && mouseX < width - 15 && mouseY >= 0 && mouseY < 10)
		{
			mc.displayGuiScreen(new GuiChat());
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_TOAST_OUT, 1));
		}
		
		if(mouseX >= width - 40 && mouseX < width - 30 && mouseY >= 0 && mouseY < 10)
		{
			mc.displayGuiScreen(new GuiStartNewChat(this));
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_TOAST_IN, 1));
		}
		
		if(mouseX >= width - 10 && mouseX < width && mouseY >= 0 && mouseY < 10)
		{
			mc.displayGuiScreen(null);
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_TOAST_OUT, 1));
		}
	}
	
	@Override
	protected void mouseClickMove(int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick)
	{
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		super.keyTyped(typedChar, keyCode);
		
		MessageList list = MessageLoader.list;
		List<String> hst = list.getAllHistories();
		
		if(keyCode == 200 && offset > 0) --offset;
		if(keyCode == 208 && offset + 3 < hst.size()) ++offset;
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
	}
}