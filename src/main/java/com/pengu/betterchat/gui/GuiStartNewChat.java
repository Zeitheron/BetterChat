package com.pengu.betterchat.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.SkinManager;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.ResourceLocation;

import com.mojang.authlib.GameProfile;
import com.pengu.betterchat.Info;
import com.pengu.betterchat.SkinGetter;
import com.pengu.betterchat.messages.Message;
import com.pengu.betterchat.messages.MessageList;
import com.pengu.betterchat.messages.MessageLoader;
import com.pengu.betterchat.tasks.TaskStartPM;
import com.pengu.hammercore.client.utils.SkinUtils;
import com.pengu.hammercore.net.HCNetwork;

public class GuiStartNewChat extends GuiScreen
{
	public static final ResourceLocation widgets = new ResourceLocation(Info.MOD_ID, "textures/widgets.png");
	
	GuiBetterChat parent;
	String user = "";
	boolean focused = false;
	int selected = -1;
	List<String> users = new ArrayList<>();
	GuiButton start;
	
	public GuiStartNewChat(GuiBetterChat gbc)
	{
		parent = gbc;
	}
	
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		parent.setGuiSize(width, height);
		parent.initGui();
		
		start = addButton(new GuiButton(0, width / 2 + 5, height / 2 - 20, 50, 20, "Start"));
		addButton(new GuiButton(1, width / 2 + 5, height / 2 + 5, 50, 20, "Cancel"));
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		start.enabled = selected != -1;
		
		parent.drawScreen(-100, -100, partialTicks);
		drawGradientRect(0, 0, width, height, 0x66666666, 0x66666666);
		
		mc.getTextureManager().bindTexture(widgets);
		drawTexturedModalRect(width / 2 - 100, height / 2 - 20, 34, 0, 100, 20);
		
		fontRenderer.drawString(user + (focused && System.currentTimeMillis() % 1000L < 500L ? "_" : ""), width / 2 - 95, height / 2 - (int) (fontRenderer.FONT_HEIGHT * 1.6), 0xFFFFFF, false);
		
		for(int i = 0; i < Math.min(16, users.size()); ++i)
		{
			String s = users.get(i);
			
			if(selected == i)
				drawGradientRect(width / 2 - 115, height / 2 + i * 18 + 11, width / 2 - 90 + fontRenderer.getStringWidth(s), height / 2 + (i + 1) * 18 + 11, 0xCC55FF55, 0xCC55FF55);
			else if(mouseX >= width / 2 - 112 && mouseY >= height / 2 + i * 18 + 11 && mouseX < width / 2 - 96 + fontRenderer.getStringWidth(s) && mouseY < height / 2 + (i + 1) * 18 + 11)
				drawGradientRect(width / 2 - 115, height / 2 + i * 18 + 11, width / 2 - 90 + fontRenderer.getStringWidth(s), height / 2 + (i + 1) * 18 + 11, 0x7755FF55, 0x7755FF55);
			
			fontRenderer.drawString(s, width / 2 - 95, height / 2 + i * 18 + 16, 0, false);
			
			GL11.glColor4f(1, 1, 1, 1);
			GlStateManager.enableBlend();
			GlStateManager.enableAlpha();
			
			try
			{
				mc.getTextureManager().bindTexture(SkinGetter.cacheSkin(s));
				GL11.glPushMatrix();
				GL11.glTranslated(width / 2 - 112, height / 2 + i * 18 + 12, 0);
				GL11.glScaled(.5, .5, .5);
				drawTexturedModalRect(0, 0, 32, 32, 32, 32);
				GL11.glPopMatrix();
			} catch(Throwable err)
			{
			}
		}
		
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(button.id == 0 && selected != -1)
		{
			String user = users.get(selected);
			String client = mc.getConnection().getGameProfile().getName();
			HCNetwork.p2p.sendTo(new TaskStartPM(client, client + " started PM history with you."), user);
			
			MessageList list = MessageLoader.list;
			if(list == null) list = new MessageList();
			list.getOrStartHistory(user).addMessage(new Message(null, "You started PM history with " + user + "."));
			
			mc.displayGuiScreen(new GuiPMChat(parent, list.getOrStartHistory(user)));
		}
		
		if(button.id == 1)
		{
			mc.displayGuiScreen(parent);
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_TOAST_OUT, 1));
		}
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		focused = mouseX >= width / 2 - 100 && mouseY >= height / 2 - 20 && mouseX < width / 2 && mouseY < height / 2;
		
		if(start.isMouseOver())
			return;
		
		selected = -1;
		for(int i = 0; i < Math.min(16, users.size()); ++i)
		{
			String s = users.get(i);
			if(mouseX >= width / 2 - 112 && mouseY >= height / 2 + i * 18 + 11 && mouseX < width / 2 - 96 + fontRenderer.getStringWidth(s) && mouseY < height / 2 + (i + 1) * 18 + 11)
			{
				selected = i;
				mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_BUTTON_CLICK, 1));
				break;
			}
		}
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		boolean changed = false;
		
		/* unfocus */
		if(keyCode == 1 && focused)
		{
			focused = false;
			return;
		}
		
		if(focused && ((typedChar >= 'a' && typedChar <= 'z') || (typedChar >= 'A' && typedChar <= 'Z') || (typedChar >= '0' && typedChar <= '9') || typedChar == '_') && user.length() < 30 && (changed = true))
			user += typedChar;
		else if(focused && keyCode == 14 && !user.isEmpty() && (changed = true))
			user = user.substring(0, user.length() - 1);
		
		if(changed)
		{
			users.clear();
			
			if(!user.isEmpty())
				mc.getConnection().getPlayerInfoMap().stream().filter(lp -> lp.getGameProfile().getName().toLowerCase().startsWith(user.toLowerCase())).forEach(loadedPlayer -> users.add(loadedPlayer.getGameProfile().getName()));
			
			return;
		}
		
		if(keyCode == 1)
		{
			mc.displayGuiScreen(parent);
			mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_TOAST_OUT, 1));
		}
	}
}