package com.pengu.betterchat.gui;

import java.io.IOException;
import java.util.List;

import org.lwjgl.opengl.GL11;

import com.pengu.betterchat.Info;
import com.pengu.betterchat.NotificationManager;
import com.pengu.betterchat.SkinGetter;
import com.pengu.betterchat.messages.Message;
import com.pengu.betterchat.messages.MessageHistory;
import com.pengu.betterchat.messages.MessageList;
import com.pengu.betterchat.messages.MessageLoader;
import com.pengu.betterchat.messages.vcm.VoiceCall;
import com.pengu.betterchat.tasks.TaskConfirmVoiceCall;
import com.pengu.betterchat.tasks.TaskPM;
import com.pengu.betterchat.tasks.TaskStartVoiceCall;
import com.pengu.betterchat.tasks.TaskStopVoiceCall;
import com.pengu.hammercore.net.HCNetwork;

import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.ResourceLocation;

public class GuiPMChat extends GuiScreen
{
	public static final ResourceLocation phoneCall = new ResourceLocation(Info.MOD_ID, "textures/phone_call.png");
	public static final ResourceLocation phoneAnsw = new ResourceLocation(Info.MOD_ID, "textures/phone_answer.png");
	public static final ResourceLocation phoneRefs = new ResourceLocation(Info.MOD_ID, "textures/phone_refuse.png");
	
	public GuiBetterChat parent;
	public GuiTextField msg;
	public MessageHistory history;
	int offset = 0;
	
	public GuiPMChat(GuiBetterChat parent, MessageHistory history)
	{
		this.parent = parent;
		this.history = history;
		offset = parent.offset;
	}
	
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
	
	@Override
	public void initGui()
	{
		super.initGui();
		
		MessageList list = MessageLoader.list;
		List<String> hst = list.getAllHistories();
		int mw = 0;
		for(int j = offset; j < Math.min(16 + offset, hst.size()); ++j)
		{
			int i = j - offset;
			String u = hst.get(j);
			mw = Math.max(mw, 38 + fontRenderer.getStringWidth(u));
		}
		
		String text = msg != null ? msg.getText() : "";
		msg = new GuiTextField(0, fontRenderer, mw + 2, height - 32, width - mw - 32, 31);
		msg.setMaxStringLength(64);
		msg.setText(text);
	}
	
	@Override
	protected void keyTyped(char typedChar, int keyCode) throws IOException
	{
		if(!msg.textboxKeyTyped(typedChar, keyCode))
			super.keyTyped(typedChar, keyCode);
		
		MessageList list = MessageLoader.list;
		List<String> hst = list.getAllHistories();
		
		int of = offset;
		if(keyCode == 200 && offset > 0) --offset;
		if(keyCode == 208 && offset + 3 < hst.size()) ++offset;
		
		if(of != offset)
		{
			int mw = 0;
			for(int j = offset; j < Math.min(16 + offset, hst.size()); ++j)
			{
				int i = j - offset;
				String u = hst.get(j);
				mw = Math.max(mw, 38 + fontRenderer.getStringWidth(u));
			}
			
			String text = msg != null ? msg.getText() : "";
			msg = new GuiTextField(0, fontRenderer, mw + 2, height - 32, width - mw - 32, 31);
			msg.setMaxStringLength(64);
			msg.setText(text);
		}
		
		if(keyCode == 28 && msg.isFocused() && !msg.getText().isEmpty())
		{
			history.addMessage(new Message(mc.getConnection().getGameProfile().getName(), msg.getText()));
			HCNetwork.p2p.sendTo(new TaskPM(mc.getConnection().getGameProfile().getName(), msg.getText()), history.user);
			msg.setText("");
		}
	}
	
	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		msg.mouseClicked(mouseX, mouseY, mouseButton);
		
		VoiceCall vc = NotificationManager.pending_calls.get(history.user);
		
		if(vc != null)
		{
			double cm = NotificationManager.currentCall != null && !NotificationManager.currentCall.remoteUser.equals(vc.remoteUser) ? .25 : mouseX >= width / 2 - 17 && mouseX < width / 2 + 15 && mouseY < 32 ? 1 : .7;
			if(cm == 1)
			{
				NotificationManager.currentCall = NotificationManager.pending_calls.remove(history.user);
				HCNetwork.p2p.sendTo(new TaskConfirmVoiceCall(null), history.user);
			}
			
			cm = NotificationManager.currentCall != null && !NotificationManager.currentCall.remoteUser.equals(vc.remoteUser) ? .25 : mouseX >= width / 2 + 17 && mouseX < width / 2 + 49 && mouseY < 32 ? 1 : .7;
			if(cm == 1)
			{
				NotificationManager.pending_calls.remove(history.user);
				vc.disconnect();
				HCNetwork.p2p.sendTo(new TaskConfirmVoiceCall("The user refused to respond"), history.user);
			}
		}
		
		if(NotificationManager.currentCall == null && mouseX >= width - 32 && mouseY < 32)
			HCNetwork.p2p.sendTo(new TaskStartVoiceCall(history.user), history.user);
		
		if(NotificationManager.currentCall != null && NotificationManager.currentCall.remoteUser.equals(history.user) && mouseX >= width - 32 && mouseY < 32)
			HCNetwork.p2p.sendTo(new TaskStopVoiceCall(null), history.user);
		
		MessageList list = MessageLoader.list;
		List<String> hst = list.getAllHistories();
		
		for(int j = offset; j < Math.min(16 + offset, hst.size()); ++j)
		{
			int i = j - offset;
			String u = hst.get(j);
			if(mouseX < 36 + fontRenderer.getStringWidth(u) + 2 && mouseY >= i * 34 && mouseY < (i + 1) * 34 && !u.equals(history.user))
			{
				parent.offset = offset;
				mc.displayGuiScreen(new GuiPMChat(parent, list.getOrStartHistory(u)));
				mc.getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_TOAST_IN, 1));
				return;
			}
		}
		
		super.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		drawGradientRect(0, 0, width, height, 0xFFFFFFFF, 0xFFFFFFFF);
		
		MessageList list = MessageLoader.list;
		List<String> hst = list.getAllHistories();
		
		int mw = 0;
		for(int j = offset; j < Math.min(16 + offset, hst.size()); ++j)
		{
			int i = j - offset;
			String u = hst.get(j);
			MessageHistory history = list.getOrStartHistory(u);
			
			try
			{
				if(history.user.equals(this.history.user))
					drawGradientRect(0, i * 34, 36 + fontRenderer.getStringWidth(u) + 2, (i + 1) * 34, 0xCC55FF55, 0xCC55FF55);
				else if(mouseX < 36 + fontRenderer.getStringWidth(u) + 2 && mouseY >= i * 34 && mouseY < (i + 1) * 34)
					drawGradientRect(0, i * 34, 36 + fontRenderer.getStringWidth(u) + 2, (i + 1) * 34, 0x7755FF55, 0x7755FF55);
			} catch(Throwable err)
			{
			}
			
			mc.getTextureManager().bindTexture(SkinGetter.cacheSkin(u));
			GL11.glPushMatrix();
			GL11.glTranslated(0, i * 34 + 1, 0);
			drawTexturedModalRect(0, 0, 32, 32, 32, 32);
			GL11.glPopMatrix();
			GL11.glColor4d(0, 0, 0, 1);
			fontRenderer.drawString(u, 36, i * 34 + 1, 0, false);
			mw = Math.max(mw, 38 + fontRenderer.getStringWidth(u));
			GL11.glColor4d(1, 1, 1, 1);
		}
		
		drawGradientRect(mw, 0, mw + 2, height, 0xCCCCCCCC, 0xCCCCCCCC);
		drawGradientRect(mw + 2, 32, width, 34, 0xCCCCCCCC, 0xCCCCCCCC);
		drawGradientRect(mw + 2, height - 34, width, height - 32, 0xCCCCCCCC, 0xCCCCCCCC);
		
		for(int i = 0; i < history.messages.size(); ++i)
		{
			Message msg = history.messages.get(i);
			int center = msg.sender == null ? (width + mw - fontRenderer.getStringWidth(msg.message)) / 2 : (msg.sender.equals(history.user) ? mw + 22 : width - 20 - fontRenderer.getStringWidth(msg.message));
			int y = height - i * 16 - 34 - fontRenderer.FONT_HEIGHT;
			fontRenderer.drawString(msg.message, center, y, 0, false);
			if(y < 0)
				break;
		}
		
		VoiceCall vc = NotificationManager.pending_calls.get(history.user);
		
		int bcc = vc != null ? 0xFFCCCCCC : 0xFFFFFFFF;
		drawGradientRect(mw + 2, 0, width, 32, bcc, bcc);
		drawGradientRect(mw + 2, height - 32, width, height, 0xFFFFFFFF, 0xFFFFFFFF);
		
		GL11.glColor4d(1, 1, 1, 1);
		mc.getTextureManager().bindTexture(SkinGetter.cacheSkin(history.user));
		GL11.glPushMatrix();
		GL11.glTranslated(mw + 2, 0, 0);
		drawTexturedModalRect(0, 0, 32, 32, 32, 32);
		GL11.glPopMatrix();
		
		GlStateManager.enableBlend();
		
		if(NotificationManager.currentCall != null && NotificationManager.currentCall.remoteUser.equals(history.user))
		{
			double cm = mouseX >= width - 32 && mouseY < 32 ? 1 : .7;
			mc.getTextureManager().bindTexture(phoneCall);
			GL11.glPushMatrix();
			GL11.glTranslated(width - 32, 0, 0);
			GL11.glScaled(2 / 16D, 2 / 16D, 2 / 16D);
			GL11.glColor3d(1 * cm, .25 * cm, .25 * cm);
			drawTexturedModalRect(0, 0, 0, 0, 256, 256);
			GL11.glPopMatrix();
		} else
		
		if(vc == null)
		{
			double cm = NotificationManager.currentCall != null ? .25 : mouseX >= width - 32 && mouseY < 32 ? 1 : .7;
			mc.getTextureManager().bindTexture(phoneCall);
			GL11.glPushMatrix();
			GL11.glTranslated(width - 32, 0, 0);
			GL11.glScaled(2 / 16D, 2 / 16D, 2 / 16D);
			GL11.glColor3d(.25 * cm, 1 * cm, .25 * cm);
			drawTexturedModalRect(0, 0, 0, 0, 256, 256);
			GL11.glPopMatrix();
		} else
		{
			double cm = NotificationManager.currentCall != null && !NotificationManager.currentCall.remoteUser.equals(vc.remoteUser) ? .25 : mouseX >= width / 2 + 17 && mouseX < width / 2 + 49 && mouseY < 32 ? 1 : .7;
			
			mc.getTextureManager().bindTexture(phoneRefs);
			
			GL11.glPushMatrix();
			GL11.glTranslated(width / 2 + 17, 0, 0);
			GL11.glScaled(2 / 16D, 2 / 16D, 2 / 16D);
			GL11.glColor3d(cm, cm, cm);
			drawTexturedModalRect(0, 0, 0, 0, 256, 256);
			GL11.glPopMatrix();
			
			cm = NotificationManager.currentCall != null && !NotificationManager.currentCall.remoteUser.equals(vc.remoteUser) ? .25 : mouseX >= width / 2 - 17 && mouseX < width / 2 + 15 && mouseY < 32 ? 1 : .7;
			
			mc.getTextureManager().bindTexture(phoneAnsw);
			
			GL11.glPushMatrix();
			GL11.glTranslated(width / 2 - 17, 0, 0);
			GL11.glScaled(2 / 16D, 2 / 16D, 2 / 16D);
			GL11.glColor3d(cm, cm, cm);
			drawTexturedModalRect(0, 0, 0, 0, 256, 256);
			GL11.glPopMatrix();
		}
		
		msg.drawTextBox();
		
		super.drawScreen(mouseX, mouseY, partialTicks);
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException
	{
	}
}