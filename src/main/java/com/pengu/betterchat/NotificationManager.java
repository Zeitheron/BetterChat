package com.pengu.betterchat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.FontRenderer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import com.pengu.betterchat.messages.vcm.VoiceCall;
import com.pengu.betterchat.tasks.TaskConfirmVoiceCall;
import com.pengu.hammercore.net.HCNetwork;

@SideOnly(Side.CLIENT)
public class NotificationManager
{
	public static final List<Notification> notifications = new ArrayList<>(32);
	public static final Map<String, VoiceCall> pending_calls = new HashMap<>();
	public static VoiceCall currentCall = null;
	public static Notification currentNotification = null;
	private static long lastSound = 0;
	public static boolean makeSound = true;
	
	public static void makeNotification(String text)
	{
		Notification not = new Notification();
		not.cutText = not.originalText = text;
		if(makeSound)
			Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsBC.PRIVATE_MESSAGE.sound, 1F));
		notifications.add(not);
	}
	
	public static void update()
	{
		if(!pending_calls.isEmpty() && System.currentTimeMillis() - lastSound >= 3500L)
		{
			lastSound = System.currentTimeMillis();
			Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundsBC.INCOMING_CALL.sound, 1));
		}
		
		List<VoiceCall> toRemove = null;
		for(VoiceCall c : pending_calls.values())
		{
			--c.timeout;
			if(c.timeout <= 0)
			{
				c.disconnect();
				HCNetwork.p2p.sendTo(new TaskConfirmVoiceCall("The user did not respond"), c.remoteUser);
				if(toRemove == null)
					toRemove = new ArrayList<>();
				toRemove.add(c);
			}
		}
		if(toRemove != null)
			pending_calls.values().removeAll(toRemove);
		
		float offset = 20;
		if(currentNotification != null)
		{
			if(currentNotification.ticksLeft > 0)
				currentNotification.ticksLeft--;
			int ticksExisted = currentNotification.maxTicksLeft - currentNotification.ticksLeft;
			if(ticksExisted <= offset)
				currentNotification.appear = ticksExisted / offset;
			if(currentNotification.ticksLeft > 0 && currentNotification.ticksLeft <= offset)
				currentNotification.appear = currentNotification.ticksLeft / offset;
			if(currentNotification.ticksLeft == 0)
				currentNotification = next();
		} else
			currentNotification = next();
	}
	
	private static Notification next()
	{
		if(notifications.isEmpty())
			return null;
		return notifications.remove(0);
	}
	
	public static class Notification
	{
		public String cutText, originalText;
		public int maxTicksLeft = 120;
		public int ticksLeft = maxTicksLeft;
		public float appear;
		
		public void cutTextToWidth(FontRenderer fr, int width)
		{
			cutText = originalText;
			String c = cutText;
			int init = cutText.length();
			int removed = 0;
			while(!cutText.isEmpty())
			{
				int w = fr.getStringWidth(cutText);
				if(w <= width)
					return;
				if(removed > init)
					break;
				if(removed == 0)
					cutText = cutText.substring(0, cutText.length() - 1);
				else
					cutText = cutText.substring(0, cutText.length() - 4);
				removed++;
				cutText += "...";
			}
		}
	}
}