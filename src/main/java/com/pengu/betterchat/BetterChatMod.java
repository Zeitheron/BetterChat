package com.pengu.betterchat;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.init.SoundEvents;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.config.GuiUtils;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ClientTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import com.pengu.betterchat.NotificationManager.Notification;
import com.pengu.betterchat.gui.GuiBetterChat;
import com.pengu.betterchat.messages.MessageList;
import com.pengu.betterchat.messages.MessageLoader;
import com.pengu.betterchat.messages.vcm.VoiceCall;
import com.pengu.hammercore.common.utils.WrappedLog;

public class BetterChatMod
{
	public static BetterChatMod scope = new BetterChatMod();
	
	public static final WrappedLog LOG = new WrappedLog(Info.MOD_NAME);
	public KeyBinding openBetterChat;
	
	public void preInit(FMLPreInitializationEvent evt)
	{
		MinecraftForge.EVENT_BUS.register(this);
		openBetterChat = new KeyBinding("key.betterchat.open.desc", Keyboard.KEY_B, "key.categories.ui");
		ClientRegistry.registerKeyBinding(openBetterChat);
	}
	
	public void init(FMLInitializationEvent evt)
	{
		Runtime.getRuntime().addShutdownHook(new Thread(()->
		{
			try
			{
				if(MessageLoader.list != null)
					MessageLoader.saveMessages();
			}
			catch(Throwable err) { err.printStackTrace(); }
		}));
	}
	
	public void postInit(FMLPostInitializationEvent evt)
	{
		
	}
	
	private boolean prevPress = false;
	private boolean lastWorldSeen = false;
	public static String lastWorldName = null;
	
	@SubscribeEvent
	public void keyPress(ClientTickEvent cte)
	{
		boolean worldSeen = Minecraft.getMinecraft().world != null;
		
		if(lastWorldSeen != worldSeen || (worldSeen && MessageLoader.list == null))
		{
			lastWorldSeen = worldSeen;
			
			if(worldSeen)
			{
				MinecraftServer mcs = FMLCommonHandler.instance().getMinecraftServerInstance();
				SocketAddress addr = Minecraft.getMinecraft().player.connection.getNetworkManager().getRemoteAddress();
				String worldName = addr.getClass().getName();
				if(mcs != null)
					worldName = mcs.getWorldName();
				else if(addr instanceof InetSocketAddress)
					worldName = ((InetSocketAddress) addr).getAddress().getHostAddress();
				lastWorldName = worldName;
			}
			
			try
			{
				if(worldSeen)
					MessageLoader.loadMessages();
				else
					try
					{
						MessageLoader.saveMessages();
						if(NotificationManager.currentCall != null)
							NotificationManager.currentCall.disconnect();
						NotificationManager.currentCall = null;
						NotificationManager.pending_calls.clear();
					} catch(Throwable err)
					{
						err.printStackTrace();
					} finally
					{
						MessageLoader.list = null;
					}
			} catch(Throwable err)
			{
				MessageLoader.list = null;
				err.printStackTrace();
			}
		}
		
		if(cte.phase == Phase.START)
			NotificationManager.update();
		boolean isPress = openBetterChat.isKeyDown();
		if(prevPress != isPress)
		{
			prevPress = isPress;
			if(isPress)
			{
				Minecraft.getMinecraft().displayGuiScreen(new GuiBetterChat());
				Minecraft.getMinecraft().getSoundHandler().playSound(PositionedSoundRecord.getMasterRecord(SoundEvents.UI_TOAST_IN, 1));
			}
		}
	}
	
	private List<String> tooltip = new ArrayList<>();
	
	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void drawOverlay(RenderGameOverlayEvent.Post evt)
	{
		if(evt.getType() == ElementType.ALL && evt.getPhase() == EventPriority.LOWEST)
		{
			ScaledResolution res = evt.getResolution();
			
			Notification n = NotificationManager.currentNotification;
			
			if(n != null)
			{
				FontRenderer fr = Minecraft.getMinecraft().fontRenderer;
				
				tooltip.clear();
				for(String s : n.cutText.split("\n"))
					tooltip.add(s);
				
				int ticksExisted = n.maxTicksLeft - n.ticksLeft;
				float left = (int) (130 - 130 * n.appear);
				
				GL11.glPushMatrix();
				GL11.glTranslated((left == 0 ? 0 : evt.getPartialTicks()) * (20 / (float) n.maxTicksLeft) * (ticksExisted < n.maxTicksLeft / 2 ? -1 : 1) + left, 0, 0);
				GuiUtils.drawHoveringText(tooltip, res.getScaledWidth() + 10, 20, res.getScaledWidth(), res.getScaledHeight(), 120, fr);
				GL11.glPopMatrix();
				
				RenderHelper.disableStandardItemLighting();
			}
		}
	}
}